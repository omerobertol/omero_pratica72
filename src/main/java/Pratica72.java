import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;
import utfpr.ct.dainf.if62c.pratica.Ocorrencias;
import utfpr.ct.dainf.if62c.pratica.OcorrenciasComparator;
import utfpr.ct.dainf.if62c.pratica.Palavras;

public class Pratica72 {
    
  public static void main(String[] args) throws FileNotFoundException, IOException {
    Scanner ler = new Scanner(System.in);
    String nomeArq;
    // solicite ao usuário que digite o nome (caminho completo) de um arquivo de texto
    nomeArq = ler.nextLine();
        
    ContadorPalavras palavras = new ContadorPalavras(nomeArq);
    
    Palavras ocorrencias = new Palavras(palavras.getPalavras());
    
    OcorrenciasComparator ordem = new OcorrenciasComparator(-1);
    ArrayList<Ocorrencias> pal = ocorrencias.ordena(ordem);  
    
    // OcorrenciasComparator ordem = new OcorrenciasComparator(-1);
    try (BufferedWriter gravaArq = new BufferedWriter(new FileWriter(nomeArq+".out"))) {
      for (int i=0; i<pal.size(); i++) {  
        gravaArq.write(pal.get(i).toString());
        gravaArq.newLine();
      }
      gravaArq.close();
    }  
  }
}