package utfpr.ct.dainf.if62c.pratica;

public class Ocorrencias implements Comparable<Ocorrencias> {
  String palavra;
  int ocorrencias;

  public Ocorrencias(String palavra, int ocorrencias) {
    this.palavra = palavra;
    this.ocorrencias = ocorrencias;
  }

  public String getPalavra() {
    return palavra;
  }

  public void setPalavra(String palavra) {
    this.palavra = palavra;
  }

  public int getOcorrencias() {
    return ocorrencias;
  }

  public void setOcorrencias(int ocorrencias) {
    this.ocorrencias = ocorrencias;
  }

  @Override
  public String toString() {
    return palavra + "," + ocorrencias;
  }

  @Override
  public int compareTo(Ocorrencias o) {
    return this.ocorrencias - o.ocorrencias;
  }

}
