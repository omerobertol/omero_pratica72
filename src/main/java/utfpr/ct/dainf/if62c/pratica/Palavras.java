package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class Palavras {
  private final HashMap<String, Integer> palavras;

  public Palavras(HashMap<String, Integer> palavras) {
    this.palavras = palavras;
  }

  public HashMap<String, Integer> getPalavras() {
    return palavras;
  }
  
  public void addPalavra(String palavra, int ocorrencias) {
    this.palavras.put(palavra, ocorrencias);
  }
  
  public ArrayList<Ocorrencias> ordena(OcorrenciasComparator ordem) {
    ArrayList<Ocorrencias> listaPalavras = new ArrayList<>();
    
    Set<String> keysTime = palavras.keySet();
    keysTime.forEach((key) -> {
        listaPalavras.add(new Ocorrencias(key, palavras.get(key)));
      });
    
    Collections.sort(listaPalavras, ordem);
   
    return listaPalavras;
  }  
    
}