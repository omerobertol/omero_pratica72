package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Character.isLetterOrDigit;
import java.util.HashMap;

public class ContadorPalavras {
  private final BufferedReader reader;    

  public ContadorPalavras(String nomeArq) throws FileNotFoundException {
    reader = new BufferedReader(new FileReader(nomeArq));    
  }
  
  public HashMap<String, Integer> getPalavras() throws IOException {
    int i, j, tam;
    HashMap<String, Integer> ocorrencias = new HashMap<>(); 
    
    BufferedReader leArq = new BufferedReader(this.reader);
    
    String linha, pal;
    while ((linha = leArq.readLine()) != null) {
      tam = linha.length();
      for (i=0; i<tam; i++) {
        j = 0;
        pal = "";
        while ((i<tam) && (isLetterOrDigit(linha.charAt(i)) == true)) {
          pal = pal + linha.charAt(i);
          i++;
          j++;
        }
        if (j != 0) {
           if (ocorrencias.containsKey(pal))
              ocorrencias.replace(pal, ocorrencias.get(pal)+1);
           else ocorrencias.put(pal, 1);
        }   
      }
    }
 
    reader.close();
    return(ocorrencias);
  }    
}
