package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

public class OcorrenciasComparator implements Comparator<Ocorrencias> {
  private final int ordem; 

  public OcorrenciasComparator(int ordem) {
    this.ordem = ordem;     // -1: descendente
                            // +1: ascendente
  }

  @Override
  public int compare(Ocorrencias o1, Ocorrencias o2) {
    if (o1.ocorrencias == o2.ocorrencias)
       // comparando por número de ocorrencias, se forem iguais, comparar pela palavra 
       if (o1.palavra.equalsIgnoreCase(o2.palavra))
          return(0);
       else return(o1.palavra.compareToIgnoreCase(o2.palavra));
    else return((o1.ocorrencias - o2.ocorrencias) * ordem); 
  }

}
